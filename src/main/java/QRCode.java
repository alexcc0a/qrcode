import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class QRCode {
    public static void main(String[] args) {
        // Создание объекта Scanner для считывания ввода с консоли.
        Scanner scanner = new Scanner(System.in);
        // Вывод сообщения для пользователя, чтобы ввести URL для генерации QR кода.
        System.out.print("Введите URL для генерации QR кода: ");
        // Считывание введенного URL.
        String url = scanner.nextLine();

        // Установка ширины и высоты QR кода.
        int width = 300;
        int height = 300;
        // Установка формата изображения (png) и пути к файлу QR кода.
        String format = "png";
        String filePath = "qrcode.png";

        // Создание объекта Map для хранения настроек кодирования QR кода.
        Map<EncodeHintType, ErrorCorrectionLevel> hints = new HashMap<>();
        // Установка уровня коррекции ошибок (L для низкого уровня).
        hints.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.L);

        // Создание объекта для кодирования QR кода.
        QRCodeWriter qrCodeWriter = new QRCodeWriter();
        BitMatrix bitMatrix;
        try {
            // Создание битовой матрицы с помощью qrCodeWriter.
            bitMatrix = qrCodeWriter.encode(url, BarcodeFormat.QR_CODE, width, height, hints);

            // Создание буферизованного изображения для QR кода.
            BufferedImage qrCodeImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
            qrCodeImage.createGraphics();

            Graphics2D graphics = (Graphics2D) qrCodeImage.getGraphics();
            graphics.setColor(Color.WHITE);
            graphics.fillRect(0, 0, width, height);
            graphics.setColor(Color.BLACK);

            // Заполнение черных пикселей соответствующими пикселями в битовой матрице.
            for (int x = 0; x < width; x++) {
                for (int y = 0; y < height; y++) {
                    if (bitMatrix.get(x, y)) {
                        graphics.fillRect(x, y, 1, 1);
                    }
                }
            }

            // Создание файла для сохранения QR кода.
            File qrCodeFile = new File(filePath);
            // Запись изображения QR кода в файл используя ImageIO.
            ImageIO.write(qrCodeImage, format, qrCodeFile);

            // Вывод сообщения пользователю о том, что QR код успешно сгенерирован и сохранен в файл.
            System.out.println("QR код успешно сгенерирован и сохранен в файл " + filePath);
        } catch (WriterException | IOException e) {
            // Вывод стека вызовов исключения, если что-то пошло не так.
            e.printStackTrace();
        }
    }
}